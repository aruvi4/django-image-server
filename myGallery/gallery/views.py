from django.shortcuts import render, redirect
from .models import Image, Emoji, EmotionAnalysisResult
from .forms import ImageUploadForm
# Create your views here.

def recognize_emotion(image) -> str:
    return "test"

def analyse_image(request, id):
    image = Image.objects.get(id=id)
    generated_emotion = recognize_emotion(image)
    generated_emoji = Emoji.objects.get(title="test")
    result = EmotionAnalysisResult.objects.create(orig_image=image, emotion=generated_emotion, emoji=generated_emoji)
    return redirect('image_list')

def image_list(request):
    results = EmotionAnalysisResult.objects.all()
    context = {'results': results}
    return render(request, "gallery/image_list.html", context)

def upload_image(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('analyse_image', id=form.instance.id)
    else:
        form = ImageUploadForm()
    context = {'form': form}
    return render(request, 'gallery/image_upload_form.html', context)