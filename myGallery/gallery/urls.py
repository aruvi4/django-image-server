from django.urls import path
from . import views

urlpatterns = [
    path('all', views.image_list, name='image_list'),
    path('upload', views.upload_image, name='upload_image'),
    path('analyse/<int:id>', views.analyse_image, name='analyse_image')
]