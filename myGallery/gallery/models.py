from django.db import models

# Create your models here.

class Image(models.Model):
    title = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='pics')

class Emoji(models.Model):
    title = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='pics')

    def __str__(self):
        return self.title

class EmotionAnalysisResult(models.Model):
    orig_image = models.ForeignKey(Image, on_delete=models.CASCADE)
    emotion = models.CharField(max_length=20)
    emoji = models.ForeignKey(Emoji, on_delete=models.CASCADE)